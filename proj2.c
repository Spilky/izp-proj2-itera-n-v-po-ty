/*
 * Soubor:  proj2.c
 * Datum:   31.10.2012
 * Autor:   David Spilka (xspilk00)
 * Projekt: Itera�n� v�po�ty, projekt �. 2 pro p�edm�t IZP  
 */
 
#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>

const double IZP_SQRT_3 = 1.7320508075688772935;        // sqrt(3)
const double IZP_E = 2.7182818284590452354;        // e
const double IZP_PI = 3.14159265358979323846;      // pi
const double IZP_2PI = 6.28318530717958647692;     // 2*pi
const double IZP_PI_2 = 1.57079632679489661923;    // pi/2
const double IZP_PI_4 = 0.78539816339744830962;    // pi/4

#define POWXA 1       //1 = k�d funkce pro v�po�et mocniny
#define ARCTG 2       //2 = k�d funkce pro v�po�et arctg
#define ARGSINH 3     //3 = k�d funkce pro v�po�et argsinh
#define HELP 4        //4 = k�d funkce pro zobrazen� n�pov�dy

#define PARM_ERR 1    //chybn� zadan� parametr
#define SIGDIG_TOO 2      //chybn� zadan� vstup

void vypis();
void printHelp();
double arctg();
double argsinh();
double powxa();
void vypis();
int sigdig();
void doParams();
void printErr();
double e_na_x();
double ln();
double loadEps();

struct PARAM {
  int funkce;
  double exponent;
  int presnost;
  double eps;
  int error;
} param;

/* Napoveda */

void printHelp(void)
{
  printf("Napoveda k programu:\n"
       "Program: Projekt c. 2 - Iteracni vypocty.\n"
       "Autor: David Spilka (xspilk00)\n"
       "Program pomoci iteracnich algoritmu pocita arcus tangens,\n"
       "argument hyperbolickeho sinu nebo mocninu (s realnym exponentem)\n"
       "hodnot zadanych na vstupu.\n"
       "po nejmene cetny\n"
       "Pouziti: proj2 --help\n"
       "         proj2 -h\n"
       "         proj2 --arctg sigdig\n"
       "         proj2 --argsinh sigdig\n"
       "         proj2 --powxa sigdig exponent\n"
       "\nsigdig - p�esnost zadan� jako po�et platn�ch cifer (hodnota: 1 - 15)\n"
       "\na - exponent (hodnota: jakekoliv realne cislo)\n"
       "Popis parametru:\n"
       "  -h                  Vypise tuto obrazovku s napovedou.\n"
       "  --help              Vypise tuto obrazovku s napovedou.\n"
       "  --arctg sigdig      Vypise vysledkdy rovnajici se arcus tangens\n"
       "                      hodnot zadanych na vstupu.\n"
       "  --argsinh sigdig    Vypise vysledkdy rovnajici se argumentu hyperbolickeho\n"
       "                      sinu hodnot zadanych na vstupu.\n"
       "  --powxa sigdig a    Vypise vysledky rovnajici se a-te mocnine hodnot\n"
       "                      zadanych na vstupu.\n"
       "Priklad pouziti:\n"
       "  proj2 -- arctg sigdig    Program vypise vysledkdy rovnajici se arcus\n"
       "                           tangens hodnot zadanych na vstupu s presnosti\n"
       "                           na pocet platnych cifer zadanych v sigdig.\n");
  return;
}

/* Konec napovedy */

/* Arcus tangens */

double arctg(double x, double eps)
{
     double vysl;
     int velke = 0;
     int zaporne = 0;
     int kol_1 = 0;
     if (x < (0.0))             //kdy� je ��slo z�porn�
     {
         x = -(x);              //tak ho p�evedu na kladn�
         zaporne = 1;           //a p�evod si pozna��m
     }
     if (x >= 1.1)               //kdy� je ��slo v�t�� jak 1.1 nebo rovno 1.1 = velke
     {
         x = 1.0/x;             //tak si z n�j ud�l�m jeho p�evr�cenou hodnotu
         velke = 1;             //a pozna��m si, �e bylo v�t�� jak 1
     }
     if (x > 0.9 && x < 1.1)
     {
         x = ((IZP_SQRT_3 * x) - 1) / (IZP_SQRT_3 + x);   //kdy� je ��slo hodnotou bl�zk� 1.0, tak ho uprav podle vzorce
         kol_1 = 1;                                       //a upravu si zapamatuj
     }
     
     double k = 1.0;
     double item;
     double sum0 = 0.0;
     double sum1 = x;
     double y = x*x*x;    //p�edpo��t�m si 3. mocninu pro 2. �len �ady
     double moc2 = x*x;   //p�edpo��t�m si 2. mocninu pro dal�� �leny �ady
     
     while(fabs(sum1 - sum0) > sum1 * eps)
     {
         k += 2.0;
         y = -(y);
         item = y / k;
         sum0 = sum1;
         sum1 += item;
         y = y * moc2;
     }
     if (velke == 1)                //kdy� je ��slo velk�
     {
         vysl = IZP_PI_2 - sum1;    //kdy� je ��slo velk� a kladn�
         if (zaporne == 1)
         {
             vysl = -(vysl);        //kdy� je ��slo velk� a z�porn�
         }
     }
     else if (kol_1 == 1)           //kdy� je ��slo kolem 1
     {
          vysl = (IZP_PI / 6) + sum1;  //kdy� je ��slo kolem 1 a kladn�
          if (zaporne == 1)          
         {
             vysl = -(vysl);        //kdy� je ��slo kolem 1 a z�porn�
         }
     }
     else                           //kdy� je ��slo mal�
     {
         vysl = sum1;               //kdy� je ��slo mal� a kladn�
         if (zaporne == 1)          
         {
             vysl = -(vysl);        //kdy� je ��slo mal� a z�porn�
         }
     }
     return vysl;
}

/* Konec arcus tangens */

/* P�irozen� logaritmus */

double ln(double x, double eps)
// pouzity rozvoj prirozeneho logaritmu je ln(x) = ((2*(1/1 * ((x-1)/(x+1))^1)) + (2*(1/3 * ((x-1)/(x+1))^3)) + (2*(1/5 * ((x-1)/(x+1))^5)) ... )
{ 
  int del = 0;
  int nas = 0;
  if (x < 1.0)                //kdy� je ��slo men�� jak 1
  {
      while(x < 1.0)          //dokud je ��slo men�� jak 1, tak ho n�sob�m e 
      {
          x *= IZP_E;
          nas += 1;           //po�et n�soben� si ukl�d�m
      }
  }
  else if (x > 10.0)          //kdy� je ��slo v�t�� jak 10
  {
      while(x > 10.0)         //dokud je ��slo v�t�� jak 10, tak ho d�l�m e
      {
          x /= IZP_E;
          del +=1;            //po�et d�len� si ukl�d�m
      }
  }

  int jmen1 = 1;
  double jmen2 = x + 1.0;
  double citat2 = x - 1.0;
  double clen1 = 1.0 / jmen1;
  double clen2 = citat2 / jmen2;
  double mocnina = clen2 * clen2;
  double sum1 = clen1 * clen2 * 2.0;
  double sum0 = 0.0;
  while(fabs(sum1 - sum0) > (sum1 * eps))
  {
      jmen1 += 2;
      clen1 = 1.0 / jmen1;
      clen2 *= mocnina;
      sum0 = sum1;
      sum1 += clen1 * clen2 * 2.0;
  }

  if (nas > 0)              //kdy� je po�et n�soben� v�t�� jak 0, tak ho ode�tu od v�sledku
  {
    sum1 -= nas;
  }
  else if (del > 0)         // kdy� je po�et delen� v�t�� jak 0, tak ho p�i�tu k v�sledku
  {
    sum1 += del;
  }
  return sum1;
}

/* Konec p�irozen� logaritmus */

/* Argument hypebolick�ho sinu */

double argsinh(double x, double eps)
//po��t�m podle vzorce argsinh (x) = ln( x + sqrt( x^2 + 1))
{
     double vysl;
     int zaporne = 0;
     if (x < 0.0)                   //kdy� je ��slo z�porn�
     {
         x = -(x);                  //tak ho p�evedu na kladn�
         zaporne = 1;               //a p�evod si ozna��m
     }
     double haf = x * x + 1.0;
     double mocnina;
     mocnina = powxa(haf, 0.5, eps);
     vysl = ln(x + mocnina, eps);
     if (zaporne == 1)              //kdy� ��slo na vstupu bylo z�porn�
     {
         vysl = -(vysl);            //tak v�sledek iter. vyp. p�evedu na z�porn�
     }
  return vysl;
}

/* Konec argument hypebolick�ho sinu */

double e_na_x(double x, double eps)
{
     double cela_c;
     double vysl = 0;
     int zaporne = 0;
     x = modf(x, &cela_c);         //rozd�l�m si ��slo na celou a desetinou ��st
     if (cela_c < 0)               //kdy� je cel� ��st z�porn�
     {
       cela_c = -(cela_c);         //tak ji p�evedu na kladnou
       zaporne = 1;                //a p�evod ozna��m
     }
     if (cela_c >= 0)              //kdy� je cel� ��st rovna 0 nebo v�t�� jak nula
     {
       vysl = 1;                   //tak do v�sledku ulo��m 1
       while(cela_c != 0)          //dokud se cel� ��st nerovn� nule
       {
         vysl *= IZP_E;            //tak ji n�sob�m e, ��m� ud�l�m e^(celou ��st x)
         cela_c--;
       }
     }
     
     //itera�n� v�po�et e^(desetinn� ��st x)
     
     double sum0 = 0.0;
     double sum1 = 1.0;
     double jmen = 1.0;
     double citat = x;
     double i = 1.0;
     while(fabs(sum1 - sum0) > (sum1 * eps))
     {
         sum0 = sum1;
         sum1 += citat / jmen;
         i++;
         jmen *= i;
         citat *= x;
     }
     if (zaporne == 1)        //kdy� bylo na vstupu ��slo x z�porn�
     {
       vysl = 1 / vysl;       //tak do v�sledku ulo��m jeho p�evr�cenou hodnotu
     }
     return sum1 * vysl;      //vyn�sob�m v�sledek e^(desetinn� ��st x) s e^(cel� ��st x)
}

/* Mocninn� funkce (s re�ln�m exponentem) */

double powxa(double x, double a, double eps)
//pou�it� vzorec: e^(a * ln(x))
{
   double mocnina;
   if (x < 0.0 || (x == 0.0 && a == 0.0))   //kdy� je x z�porn� a taky, kdy� je x i exponent 0, tak v�sledek je NAN
   {
     mocnina = NAN;
   }
   else if (x == 0.0)       //kdy� x je 0, tak v�sledek je 0
   {
     mocnina = 0;
   }
   else if (a == 0.0)       //kdy� exponent je 0, tak v�sledek je 1
   {
     mocnina = 1;
   }
   else if (x == 0.0 && a < 0.0)
   {
     mocnina = INFINITY;
   }
   else
   {
     int presn_ln = DBL_DIG;
     double eps_ln = loadEps(presn_ln);
     double ln_x;
     double xko;
     ln_x = ln(x, eps_ln);
     xko = a * ln_x;
     mocnina = e_na_x(xko, eps);
   }
   return mocnina;
}

/* Konec mocninn� funkce (s re�ln�m exponentem) */

/* Vypis vysledku */

void vypis(double vysl)
{
     printf("%.10e\n", vysl);
     return;
}

/* Konec vypis vysledku */

/* Nacteni presnosti */

int sigdig(char *argv[])
{
    int presnost;
    int overeni;
    overeni = sscanf(argv[2], "%d", &presnost);   //na�ten� sigdig
    if (presnost <= 0 || overeni == 0)            //kdy� je sigdig neplatn�
    {
       printErr(PARM_ERR);                        //tak vypi� chybu
    }
    else if (presnost > DBL_DIG)                  //kdy� je sigdig v�t�� jak po�et desetinn�ch m�st prom�nn� typu double
    {
        presnost = DBL_DIG;                       //tak do sigdig vlo� 15
        printErr(SIGDIG_TOO);                     //a vypi� chybu
        return presnost;
    }
    else
    {
        return presnost;
    }
    return 0;
}

/* Konec nacteni presnosti */

/* P�evod sigdig na eps */

double loadEps(int presnost)
{
  double eps = 1.0;
  while(presnost != 0)
  {
    presnost--;
    eps *= 0.1;
  }
  return eps;
}

/* Konec p�evod sigdig na eps */

/* Chybov� hl�en� */

void printErr(int err)
{
  switch(err) {
    case 1:
      fprintf(stderr, "\nChyba v zadani parametru!\n");
      param.error = 1;
      break;
    case 2:
      fprintf(stderr, "Zadal ste moc velky sigdig, ten byl upraven na sigdig = 15\n\n");
      break;
    default:
      break;
  }
}

/* Konec chybov� hl�en� */

/* Zpracovani parametru */

void doParams(int argc, char *argv[])
//Funkce zpracov�v� parametry a p�i spr�vn� zadan�ch parametrech ulo�� do param.funkce
//k�d funkce, kter� se pot� spust�. Pokud nejsou parametry zad�ny spr�vn�, vyp�e chybu
{
  if (argc == 2)
  {
     if ((strcmp("-h", argv[1]) == 0) || (strcmp("--help", argv[1]) == 0))
     {
     param.funkce = HELP;
     }
     else
     {
        printErr(PARM_ERR);
     }
  }
  else if (argc == 3)
  {
     if (strcmp("--arctg", argv[1]) == 0)
     {
        param.presnost = sigdig(argv);
        param.funkce = ARCTG;
     }
     else if (strcmp("--argsinh", argv[1]) == 0)
     {
        param.presnost = sigdig(argv);
        param.funkce = ARGSINH;
     }
     else
     {
        printErr(PARM_ERR);
     }
  }
  else if (argc == 4) 
  {
     if (strcmp("--powxa", argv[1]) == 0)
     {
        int overeni;
        overeni = sscanf(argv[3], "%lf", &param.exponent);
        if (overeni == 0)
        {
           printErr(PARM_ERR);
        }
        else
        {
           param.presnost = sigdig(argv);
           param.funkce = POWXA;
        }
     }
     else
     {
        printErr(PARM_ERR);
     }
  }
  else
  {
     printErr(PARM_ERR);
  }
  return;
}

/* Konec zpracovani parametru */

/* Hlavni blok programu */

int main(int argc, char *argv[])
{
  param.error = 0;
  doParams(argc, argv);
  if(param.funkce == HELP)               //pokud volan� funkce souhlas� s k�dem funkce napov�dy
  {
    printHelp();                         //tak vypi� n�pov�du
  }
  else if(param.error == 0)              //kdy� nenastala ��dn� chyba
  {
    double cislo;
    double vysledek;
    int overeni;
    int blbost;
    while((overeni = scanf("%le", &cislo)) != EOF)   //tak na��tej znaky, dokud nen� zaznamen�n konec vstupu
    {
      if(overeni == 0)                               //pokud je znak neplatn�
      {
        blbost = scanf("%*s");                       //tak ho jakoby p�esko��
        vysledek = NAN;                              //do v�sledku ulo�� NAN
        vypis(vysledek);                             //a v�sledek vypi�
      }
      else if(cislo == INFINITY)                     //kdy� je ��slo na vstupu "nekone�no"
      {
        vysledek = INFINITY;                         //tak do v�sledku ulo� INF
        vypis(INFINITY);                             //a v�sledek vypi�
      }
      else
      {
      switch(param.funkce) 
      {
        case 1:                                      //pokud k�d volan� funkce souhlas� s k�dem mocninn� funkce, tak ji prove�
          param.eps = loadEps(param.presnost);
          vysledek = powxa(cislo, param.exponent, param.eps);
          vypis(vysledek);
          break;
        case 2:                                      //pokud k�d volan� funkce souhlas� s k�dem funkce arkus tangens, tak ji prove�
          param.eps = loadEps(param.presnost);   
          vysledek = arctg(cislo, param.eps);
          vypis(vysledek);
          break;
        case 3:                                      //pokud k�d volan� funkce souhlas� s k�dem funkce argument hyperbolick�ho sinu, tak ji prove�
          param.eps = loadEps(param.presnost);   
          vysledek = argsinh(cislo, param.eps);
          vypis(vysledek);
          break;
        default:
          break;
      }
      }
    }
  }
  return 0;
}

/* Konec hlavniho bloku programu */
